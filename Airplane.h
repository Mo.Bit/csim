#pragma once

#include "View.h"
#include "MyTimer.h"

#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QObject>

class Airplane : public QObject, public QGraphicsPixmapItem {
    Q_OBJECT
public:
    Airplane();

private slots:
    void toMove();
    void move();

private:
    qreal _firstPosX = 0;
    qreal _firstPosY = 0;
    qreal _width = 0;
    qreal _height = 0;
    MyTimer* _toMoveTimer = nullptr;
    MyTimer* _moveTimer = nullptr;
};
