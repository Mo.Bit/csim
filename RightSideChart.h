#pragma once

#include "MyQChart.h"
#include "ChartDialog.h"

#include <QtCharts/QSplineSeries>
#include <QtCharts/QChartView>

#include <QGraphicsItem>
#include <QMargins>
#include <QGraphicsSceneMouseEvent>

/**
 * @brief The Chart class for the right side chart
 */
class RightSideChart: public MyQChart {
    Q_OBJECT
public:
    /**
     * @brief Chart sets series, axis, titles, colors, width, removes margins and hides legend
     * @param parent for parentship
     */
    RightSideChart(QGraphicsItem* const parent);

    QtCharts::QSplineSeries* dialogSeries1() const {return _dialogSeries1;}
    QtCharts::QSplineSeries* dialogSeries2() const {return _dialogSeries2;}

public slots:
    /**
     * @brief update appends new values, scrolls x axis and adjusts y range
     * @param y1 new value for number of reflections
     * @param y2 new value for number of CO2 particles
     */
    void update(const int &y1, const int &y2);

private:
    int _x = -2;

    // y up range
    int _y1UpRange = 50;
    int _y2UpRange = _y1UpRange;

    int _biggestY1Value = _y1UpRange;
    int _biggestY2Value = _y2UpRange;

    // series for ChartDialog
    QtCharts::QSplineSeries* _dialogSeries1 = nullptr;
    QtCharts::QSplineSeries* _dialogSeries2 = nullptr;

    ChartDialog* _chartDialog = nullptr;

    QtCharts::QChartView* _chartView = nullptr;

    void checkYRange(const int& y1, const int& y2);

    void mousePressEvent(QGraphicsSceneMouseEvent* event);
};
