#include "Particle.h"

#include "View.h"

std::list<Particle*> Particle::_cParticles;
const std::array<QColor, 4> Particle::_pColors = {Qt::black, Qt::blue, Qt::red, Qt::gray};
qreal Particle::_diameter;
qreal Particle::_pRadius;
qreal Particle::_particleUpperYLimit;
qreal Particle::_particleLowerYLimit;

void Particle::setupAndAdd() {
    Particle::_cParticles.clear();

    Particle::_pRadius = View::get()->widthPart() * 0.022;
    Particle::_diameter = Particle::_pRadius * 2;
    Particle::_particleUpperYLimit = View::get()->heightPart() * 1.5;
    Particle::_particleLowerYLimit = View::get()->heightPart() * 4;


    Particle::addP(View::get()->width() * (Particle::_particleLowerYLimit - Particle::_particleUpperYLimit)
                   / pow(Particle::_pRadius * 20, 2));
}

Particle* Particle::takeOneCParticle() {
    if (!Particle::_cParticles.empty()){
        Particle* const tmp = Particle::_cParticles.front();
        Particle::_cParticles.pop_front();
        return tmp;
    } else {
        // no CO2 Particles to take
        return nullptr;
    }
}

void Particle::addP(const unsigned int &inputNum) {
    unsigned int num;
    bool isC; // CO2
    if (inputNum > 0) {
        // on start
        num = inputNum;
        isC = false; // not CO2
    } else {
        // adding CO2 (by addCButton)
        const int numOfCToAdd = 10;
        const int numCParticles = Particle::_cParticles.size();
        if (numCParticles >= Particle::_manuallyAddCLimit) {
            // limit reached
            View::get()->addCButton_setDisabled(true); // gets activated in createSunLight()
            return;
        } else if (numCParticles > Particle::_manuallyAddCLimit - numOfCToAdd) {
            // add only to limit
            num = Particle::_manuallyAddCLimit - numCParticles;
        } else {
            // not near limit
            num = numOfCToAdd;
        }
        isC = true; // CO2
    }

    QColor color;
    for (unsigned int i = 0; i < num; i++) {
        if (!isC) {
            // not CO2 (untill now)
            color = _pColors[QRandomGenerator::global()->bounded(int(_pColors.size()))];
        } else {
            // CO2
            color = Qt::black;
        }

        new Particle(0,0, color, false);
    }
}

Particle* Particle::produce(const qreal &x, const qreal &y) {
    return new Particle(x,y, Qt::black, true);
}

void Particle::toMoveToTree(const qreal &distanceToTree, const MovementUnitVector &vectToTree,
                            Tree* const inAbsorbTree) {
    _disatanceToTree = distanceToTree;

    // changing the move vector of the particle
    _vect = vectToTree;

    _absorbTree = inAbsorbTree;
    _absorbTreeIndex = _absorbTree->ownIndex();

    // new move connection
    _moveTimer->stop();
    disconnect(_moveConnection);

    _moveConnection = connect(_moveTimer, SIGNAL(timeout()),
                              this, SLOT(moveToTree()));
    _moveTimer->customStart(Particle::_timerInterval);
}

void Particle::move() {
    const qreal correction = Particle::_pRadius + 1; // to avoid problems

    qreal newX = x();
    qreal newY = y();
    bool moveNormally = true;
    if (newX >= View::get()->scene()->width()) {
        // out of range on the right side
        _vect.invertX();
        newX -= correction;
        moveNormally = false;
    } else if (newX + Particle::_diameter <= 0) {
        // out of range on the left side
        _vect.invertX();
        newX += correction;
        moveNormally = false;
    }

    if (newY + Particle::_diameter >= Particle::_particleLowerYLimit) {
        // reached the top (limit) in the atmosphere
        _vect.invertY();
        newY -= correction;
        moveNormally = false;
    } else if (newY <= Particle::_particleUpperYLimit) {
        // reached the bottom (limit) in the atmosphere
        _vect.invertY();
        newY += correction;
        moveNormally = false;
    }

    if (moveNormally) {
        // normal center movement
        newX += _vect.x() * 3;
        newY += _vect.y() * 3;
    }
    setPos(newX, newY);
}

void Particle::moveUp() {
    const qreal oldY = y();
    if (oldY + Particle::_diameter < Particle::_particleLowerYLimit) {
        // arrieved
        disconnect(_moveConnection);

        // new move vect for the movement in the atmosphere
        _vect.generateRandomUnitVector();

        // start movement in the atmosphere
        toMove();

        // add to cParticles
        Particle::_cParticles.push_back(this);
    } else {
        // on the way up
        setPos(x(), oldY - 10);
    }
}

void Particle::moveToTree() {
    if (Tree::treePointerOnIndex(_absorbTreeIndex) != _absorbTree) {
        // the tree is not there anymore (felled) -> move up again to the atmosphere
        // stop current movement
        _moveTimer->stop();
        disconnect(_moveConnection);

        _moveConnection = connect(_moveTimer, SIGNAL(timeout()),
                                  this, SLOT(moveUp()));
        _moveTimer->customStart(Particle::_timerInterval);
    } else {
        // the tree is still there (not felled)
        if (_disatanceToTree > Particle::_moveToTreeStep - 5) {
            // on the way
            // move
            moveBy(_vect.x() * Particle::_moveToTreeStep, _vect.y() * Particle::_moveToTreeStep);
            _disatanceToTree -= Particle::_moveToTreeStep;
        } else {
            // reached the tree
            // stop movement
            _moveTimer->stop();
            disconnect(_moveConnection);

            View::get()->removeFromScene(this);
            _absorbTree->cArrieved();
            delete this;
            return;
        }
    }
}

Particle::Particle(qreal x, qreal y, const QColor &color, const bool &producedDown):
    FadableItem(this, View::get()),
    _moveTimer(new MyTimer(this))
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);

    if (!producedDown) {
        // from atmosphere
        // random center position
        x = QRandomGenerator::global()->bounded(View::get()->scene()->width());
        y = (Particle::_particleUpperYLimit + Particle::_particleLowerYLimit) / 2;
        setPos(x-Particle::_pRadius, y-Particle::_pRadius);
        setRect(0, 0,
                2*Particle::_pRadius, 2*Particle::_pRadius);

        _vect.generateRandomUnitVector(); // random unit vector

        setBrush(QBrush(color)); // color

        fadeIn();

        if (color == Qt::black) {
            // CO2
            Particle::_cParticles.push_back(this);
        }

        // start movement in the atmosphere
        toMove();
    } else {
        // produced down
        // given center position
        setPos(x - Particle::_pRadius, y - Particle::_pRadius);
        setRect(0, 0,
                Particle::_pRadius * 2, Particle::_pRadius * 2);
        setBrush(QBrush(color)); // color

        View::get()->addToScene(this); // add to scene

        // start movement up to the atmosphere
        _moveConnection = connect(_moveTimer, SIGNAL(timeout()),
                                  this, SLOT(moveUp()));
        _moveTimer->customStart(Particle::_timerInterval);
    }
}

void Particle::toMove() {
    // connection to move()
    _moveConnection = connect(_moveTimer, SIGNAL(timeout()),
                              this, SLOT(move()));
    _moveTimer->customStart(Particle::_timerInterval);
}
