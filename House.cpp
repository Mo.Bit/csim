#include "House.h"

#include "View.h"

House::House(const qreal& x, const qreal& y, QObject* const parent, const bool &oneOfFirstHouses):
    FadableItem(this, parent),
    QGraphicsRectItem(x, y,
                      View::get()->widthPart() * 0.4, View::get()->heightPart() * 0.3),
    _bodyOutline(new House::BodyOutline(x, y, this)),
    _roof(new House::Roof(x,y,this))
{
    const qreal height = View::get()->heightPart() * 0.4;
    _tower = new CTower(x + View::get()->widthPart() * 0.05, y - height,
                        View::get()->widthPart() * 0.05, height,
                        oneOfFirstHouses, 0.5, this, 8000);
    setBrush(QBrush(QColor(165,42,42)));
    setZValue(1);
    fadeIn();
}

House::BodyOutline::BodyOutline(const qreal& x, const qreal& y, QObject* const parent):
    FadableItem(this, parent),
    QGraphicsRectItem(x, y,
                      View::get()->widthPart() * 0.4, View::get()->heightPart() * 0.3)
{
    setZValue(3);
    fadeIn();
}

House::Roof::Roof(const qreal& x, const qreal& y, QObject* const parent):
    FadableItem(this, parent)
{
    QPolygonF polygon;
    polygon << QPointF(x, y)
            << QPointF(x + View::get()->widthPart() * 0.2, y - View::get()->heightPart() * 0.3)
            << QPointF(x + View::get()->widthPart() * 0.4, y);
    setPolygon(polygon);

    setBrush(QBrush(Qt::black));
    setZValue(1);
    fadeIn();
}
