#pragma once

#include "FadableItem.h"
#include "MyTimer.h"

#include <QGraphicsRectItem>
#include <QMetaObject>

class CTower : public FadableItem, public QGraphicsRectItem {
    Q_OBJECT
public:
    CTower(const qreal &factor, QObject* const parent);

    CTower(const qreal &x, const qreal &y,
           const qreal &width, const qreal &height,
           const bool &sendfirstHouseTimer, const qreal &zValue, QObject* const parent,
           const int &timerInterval = 2000);

private slots:
    void produceC();

private:
    qreal _towerWidth;

    MyTimer* _towerTimer = nullptr;
    QMetaObject::Connection _produceCConnection;

    void continueConstruction(const int &timerInterval = 2000, const qreal &zValue = 2,
                              const bool &sendFirstHouseTimer = false);
};
