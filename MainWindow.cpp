#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent)
{
    setWindowTitle("CSim v0.2"); // window title

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding); // expanding

    // set shortcuts
    new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Q), this, SLOT(close()));


    auto splitter = new QSplitter(this);
    splitter->setHandleWidth(3);
    setCentralWidget(splitter);

    // show
    showFullScreen();

    auto rightSideWidget = new QWidget(splitter);
    auto view = new View(rightSideWidget, splitter);

    splitter->addWidget(view);
    splitter->addWidget(rightSideWidget);
    splitter->setCollapsible(0, false);
}

void MainWindow::closeEvent(QCloseEvent* event) {
    event->accept();
    QApplication::exit(0);
}
