#pragma once

#include "MyTimer.h"

#include <QObject>
#include <QGraphicsPolygonItem>
#include <QMetaObject>
#include <QPolygonF>
#include <QPointF>
#include <QBrush>

class Ice : public QObject, public QGraphicsPolygonItem {
    Q_OBJECT
public:
    Ice(QObject* const parent);

    void startMelting();

private slots:
    void melt();

private:
    MyTimer* _meltTimer = nullptr;
    QMetaObject::Connection _meltConnection;

    unsigned int _iceStep = 3;
    unsigned int _iceTimerInterval = 1000;
};
