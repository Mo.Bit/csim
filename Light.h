#pragma once

#include "View.h"
#include "MovementUnitVector.h"
#include "MyTimer.h"

#include <QGraphicsLineItem>
#include <QObject>
#include <QPointF>
#include <QMetaObject>
#include <QColor>
#include <QPen>
#include <QLineF>
#include <QList>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsEllipseItem>
#include <QRectF>

class Light : public QObject, public QGraphicsLineItem {
    Q_OBJECT

    enum LightMode {
        ShortWave, LongWave
    };

    enum DevelopmentMode {
        Grow, Shrink
    };

public:
    /**
     * @brief Light sets movement unit vector, adds to scene and starts growing
     * @param num number of light out of all
     * @param all number of all lights to generate (from sun)
     * @param inLightMode light mode (wave)
     * @param fromSun wether from sun or not
     * @param x1 coordinate of first point
     * @param y1 coordinate of first point
     * @param x2 coordinate of second point
     * @param y2 coordinate of second point
     */
    Light(const qreal &num, const qreal &all, const LightMode &inLightMode, const bool &fromSun = false,
          const qreal &x1 = 0, const qreal &y1 = 0,
          const qreal &x2 = 0, const qreal &y2 = 0);

    ~Light();

public slots:
    /**
     * @brief move moves and reflects light
     */
    void move();

    /**
     * @brief grow grows and then moves light
     */
    void grow();

    /**
     * @brief shrink shrinks and then deletes light
     */
    void shrink();

    /**
     * @brief createSunLight checks and updates reflection rate and number of CO2 particles, then creates sun light
     */
    static void createSunLight();

private:
    static const qreal _lightStep; // step that light makes on movement
    const unsigned int _lightTimerInterval = 50; // speed for the timer

    static const unsigned int _lightsNum = 20; // value for number of lights (not same as seen in the app)
    static unsigned int _numReflectedDown; // number of lights reflected down between two sun light creations

    MovementUnitVector _vect; // unit movement vector

    // timer and connection
    MyTimer* _lightTimer = nullptr;
    QMetaObject::Connection _lightConnection;

    const unsigned int _numGrowParts = View::get()->heightPart() / _lightStep; // heightPart is the length of a light line
    unsigned int _devLevel; // development level (while growing or shrinking)
    qreal _shrinkP1X, _shrinkP1Y, _growP1X, _growP1Y; // points for development

    LightMode _lightMode; // light mode (wave)

    QGraphicsLineItem* _collisionLine = nullptr; // to check colliding items

    /**
     * @brief develop sets point for development and redirects to grow() or shrink()
     * @param dev to direct to grow() or shrink()
     */
    void develop(const DevelopmentMode &dev);
};
