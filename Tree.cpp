#include "Tree.h"

#include "View.h"
#include "Particle.h"
#include "Factory.h"
#include "Dim2Vector.h"
#include "MovementUnitVector.h"

#include <cmath>

qreal Tree::_rectWidth;
std::vector<bool> Tree::_trees;
std::vector<Tree*> Tree::_treesPointers;
unsigned int Tree::_numTrees = 0;
const int Tree::_takeCTimerInterval = 10000;

const unsigned int Tree::_maxNumOfTrees = 30;
const unsigned int Tree::_treesLimitToFactory = 23;
const unsigned int Tree::_treesLimitToHouses = 17;

Tree::Tree(const unsigned int &index):
    FadableItem(this, View::get()),
    _ownIndex(index),
    _treeTimer(new MyTimer(this)),
    _treeConnection(connect(_treeTimer, SIGNAL(timeout()),
                            this, SLOT(takeC())))
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);

    const qreal xDis = Tree::_rectWidth + index * Tree::_rectWidth * 2.12;
    setPos(xDis, View::get()->heightPart() * 8.9);
    setRect(0, 0,
            Tree::_rectWidth, View::get()->heightPart() * 0.6);
    setBrush(QBrush(QColor(139,69,19)));
    setZValue(2.1);

    _circle = new TreeEllipse(xDis - Tree::_rectWidth, View::get()->heightPart() * 8.9 - Tree::_rectWidth * 2.5,
                              Tree::_rectWidth * 3, Tree::_rectWidth * 3, this);

    fadeIn();

    _treeTimer->setSingleShot(true);
    _treeTimer->customStart(QRandomGenerator::global()->bounded(Tree::_takeCTimerInterval));
}

void Tree::setupAndAdd() {
    Tree::_rectWidth = View::get()->widthPart() * 0.075;
    Tree::_numTrees = 0;

    Tree::_trees.clear();
    Tree::_trees.resize(_maxNumOfTrees, false);

    Tree::_treesPointers.clear();
    Tree::_treesPointers.resize(_maxNumOfTrees, nullptr);

    // add trees at start
    for (unsigned int i = 0; i < 5; i++) {
        Tree::addTree();
    }
}

void Tree::addTree() {
    auto it = std::find(Tree::_trees.begin(), Tree::_trees.end(), false);
    if (it != Tree::_trees.end() && !Tree::noPlace()) {
        {
            const int freeIndex = std::distance(Tree::_trees.begin(), it);
            Tree::_treesPointers[freeIndex] = new Tree(freeIndex);
            Tree::_trees[freeIndex] = true;
        }
        Tree::_numTrees++;

        if (Tree::noPlace()) {
            View::get()->addTreeButton_setDisabled(true);
            View::get()->addToLog(tr("Max. number of trees reached!"));
        }
    }
}

void Tree::takeC() {
    if (_cTaken < 5) {
        Particle* const c = Particle::takeOneCParticle();
        if (c != nullptr) {
            const Dim2Vector lenVectToTree(c->x() + Particle::pRadius(), c->y() + Particle::pRadius(),
                                           _circle->x() + _circle->rect().width() / 2, _circle->y() + _circle->rect().height() / 2);

            const MovementUnitVector unitVectToTree(lenVectToTree.x(), lenVectToTree.y());

            c->toMoveToTree(lenVectToTree.length(), unitVectToTree, this);
        } else {
            // no cParticle to take
            _treeTimer->customStart(Tree::_takeCTimerInterval); // single shot
        }
    } else {
        disconnect(_treeConnection);
        _treeConnection = connect(_treeTimer, SIGNAL(timeout()),
                                  this, SLOT(toDie()));
        _treeTimer->customStart(Tree::_takeCTimerInterval); // single shot
    }
}

void Tree::cArrieved() {
    moveVertically(-0.1);

    _cTaken++;
    _treeTimer->customStart(Tree::_takeCTimerInterval); // still SINGLE SHOT! -> takeC
}

bool Tree::noPlace() {
    return (Tree::_numTrees >= Tree::_treesLimitToHouses && View::get()->newHousesBuilt()) ||
            (Tree::_numTrees >= Tree::_treesLimitToFactory && View::get()->factory()->factoryLevel() > 0) ||
            (Tree::_numTrees >= Tree::_trees.size() && View::get()->factory()->factoryLevel() == 0);
}

void Tree::moveVertically(const qreal &factor) {
    const qreal dy = View::get()->heightPart() * factor;
    moveBy(0, dy);
    _circle->moveBy(0, dy);
}

void Tree::fell(const unsigned int& limit) {
    for (unsigned int i = limit + 1; i < Tree::_trees.size(); i++) {
        if (Tree::_trees[i]) {
            Tree* const t = Tree::_treesPointers[i];
            Tree::_treesPointers[i] = nullptr;
            t->_felled = true;
            t->toDie();
        }
    }
}

void Tree::toDie() {
    _treeTimer->stop();
    disconnect(_treeConnection);

    _treeTimer->setSingleShot(false);
    _treeConnection = connect(_treeTimer, SIGNAL(timeout()),
                             this, SLOT(die()));
    _treeTimer->customStart(500);
}

void Tree::die() {
    if (_circle->y() < View::get()->heightPart() * 9) {
        // still dying
        moveVertically(0.1);
    } else {
        // under ground
        _treeTimer->stop();
        disconnect(_treeConnection);

        if (_felled) {
            Tree::_trees[_ownIndex] = false;
            Tree::_treesPointers[_ownIndex] = nullptr;
            Tree::_numTrees--;

            if (!noPlace()) {
                View::get()->addTreeButton_setDisabled(false);
            }
        } else {
            Tree::_treesPointers[_ownIndex] = new Tree(_ownIndex);
        }

        delete this;
        return;
    }
}

Tree::TreeEllipse::TreeEllipse(const qreal& x, const qreal& y, const qreal& w, const qreal& h, QObject* const parent):
    FadableItem(this, parent)
{
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);

    setPos(x, y);
    setRect(0,0, w,h);
    setBrush(QBrush(QColor(0,128,0)));
    setZValue(2.2);
    fadeIn();
}
