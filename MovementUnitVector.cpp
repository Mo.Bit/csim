#include "MovementUnitVector.h"

MovementUnitVector::MovementUnitVector(const qreal &x1, const qreal &y1, const qreal &x2, const qreal &y2):
    Dim2Vector(x1,y1, x2,y2)
{
    makeUnit();
}

MovementUnitVector::MovementUnitVector(const qreal& inX, const qreal& inY)
{
    _x = inX;
    _y = inY;
    makeUnit();
}

void MovementUnitVector::generateRandomUnitVector() {
    do {
        // random vector
        _x = QRandomGenerator::global()->bounded(-100,101);
        _y = QRandomGenerator::global()->bounded(-100,101);
    } while (_x == 0.0 || _y == 0.0); // to avoid moving only horizontally or only vertically
    makeUnit(); // make the vector an unit vector
}
