#include "MyTimer.h"

#include <QDebug>

std::list<MyTimer*> MyTimer::_allTimers;
bool MyTimer::_paused = false;
bool MyTimer::_boosed = false;

MyTimer::MyTimer(QObject* const parent):
    QTimer(parent)
{
    MyTimer::_allTimers.push_front(this);
}

MyTimer::~MyTimer() {
    auto it = MyTimer::_allTimers.begin();
    {
        const auto end = MyTimer::_allTimers.end();
        if (it == end) {
            // list empty
            return;
        }

        while((*it) != this) {
            it++;
            if (it == end) {
                qDebug() << "Trying to delete a timer not existing in allTimers!\n";
                return;
            }
        }
    }
    MyTimer::_allTimers.erase(it);
}

void MyTimer::setup() {
    MyTimer::_paused = false;
    MyTimer::_boosed = false;
}

void MyTimer::setPaused(const bool& b) {
    MyTimer::_paused = b;

    if (b) {
        for (MyTimer* t : MyTimer::_allTimers) {
            t->stop();
        }
    } else {
        for (MyTimer* t : MyTimer::_allTimers) {
            t->customStart();
        }
    }
}

void MyTimer::customStart(const int& msec) {
    if (msec >= 0) {
        customSetInterval(msec);
    }

    if (MyTimer::_paused) {
        return;
    }
    start();
}

void MyTimer::customSetInterval(const int& msec) {
    if (msec >= 2) { // under 2 would make problems by boost (stucking at 0)
        if (MyTimer::_boosed) {
            _interval = msec / 2;
        } else {
            _interval = msec;
        }

        setInterval(_interval);
    } else {
        qDebug() << "Interval < 2: Trying to set unvalid interval to MyTimer!\n";
    }
}

void MyTimer::setBoosted(const bool &b) {
    MyTimer::_boosed = b;

    if (b) {
        for (MyTimer* t : MyTimer::_allTimers) {
            t->_interval /= 2;
            t->setInterval(t->_interval);
        }
    } else {
        for (MyTimer* t : MyTimer::_allTimers) {
            t->_interval *= 2;
            t->setInterval(t->_interval);
        }
    }
}
