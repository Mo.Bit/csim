# ToDo

## Add
- [ ] **Docs in the code (in progress)**
- [ ] **Extended Scene (in progress)**
- [ ] Ice bear
	- [ ] Pixmap
	- [ ] Move up when melting
	- [ ] Die
- [ ] Check boxes to activate/deactivate elements
- [ ] H2O Particles after ice melting?
- [ ] Cow
	- [ ] Design
	- [ ] Produce methan
	- [ ] Move/eat? -> grass?
- [ ] Airplane
	- [ ] Design
	- [ ] Produce CO2
- [ ] Car
	- [ ] Design
	- [ ] Produce CO2
- [ ] Thermometer?
- [ ] "Planet Over!" (instead of "Game Over")
	- [ ] Set limit of reflections
	- [ ] Show message
	- [ ] Dialog to close or restart (or continue?)
- [ ] Longwaves from sun
- [ ] Renew Ice (as "illegal move")
- [ ] Settings and more
	- [ ] Performance settings
		- [ ] Set number of lights
		- [ ] Set speed of Light
	- [ ] Text size
	- [ ] Contact
- [ ] README & "Tutorial"
	- [ ] pdf file for explaining functionality
- [ ] Translation to German
- [ ] Icon
- [ ] Zoom?
- [ ] How to deploy the software?
- [x] Display warnings/messages in the ~~warningsLabel~~ log
- [x] Hints (Tooltips)
- [x] Buttons
	- [x] Button for pause
		- [x] Stop timers
		- [x] Restart timers
	- [x] Button for close or restart -> Clean restart: make sure everything gets deleted
	- [x] Button to speed up
- [x] Splitter on the right side, especially to make the chart bigger
- [x] Big chart as Dialog on Click on Chart
- [x] GPLv3 License
- [x] Fell trees after Ice melting for new houses
- [x] Houses producing CO2
- [x] Legend for particles

## Fix
- [x] Stop the little scrolling!
- [x] Stop CO2 moving to a felled tree on the way and not near the ground where the tree was

## Change
- [ ] Ice melting stoppable
- [x] Particles reflection instead of spawning on other side
- [x] make all variables private -> getters & setters
- [x] Expand atmosphere
- [x] color of Ice
- [x] Trees renewing themselves
