#include "HistoryChart.h"

HistoryChart::HistoryChart(QGraphicsItem* const parent, QtCharts::QSplineSeries* const series1, QtCharts::QSplineSeries* const series2,
                           const int& biggestY1Value, const int& biggestY2Value):
    MyQChart(parent, series1, series2)
{
    setBiggerY1Range(biggestY1Value);
    setBiggerY2Range(biggestY2Value);
}

void HistoryChart::setBiggerY1Range(const int& biggestY1Value) {
    _y1Axis->setMax(biggestY1Value);
}

void HistoryChart::setBiggerY2Range(const int& biggestY2Value) {
    _y2Axis->setMax(biggestY2Value);
}

void HistoryChart::resizeX(const int& lastX) {
    _xAxis->setMax(lastX + 1);
    _xAxis->setTickCount(lastX + 2);
}
