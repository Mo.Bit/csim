#include "Factory.h"

#include "View.h"
#include "Tree.h"

#include <QDebug>

Factory::Factory(QObject* const parent):
    FadableItem(this, parent)
{
    setRect(QRectF(View::get()->widthPart() * 4, View::get()->heightPart() * 7.8,
                   View::get()->widthPart(), View::get()->heightPart() * 1.2));
    setBrush(QBrush(Qt::black));
    setZValue(2);

    _towers.reserve(3);
}

void Factory::createOrUpgradeFactory() {
    if (_factoryLevel > 2) {
        qDebug() << "Factory already on max. level!\n";
        return;
    }

    if (_factoryLevel == 0) {
        // creating factory
        fadeIn();

        Tree::fell(Tree::treesLimitToFactory());

        View::get()->destroyOrDowngradeFactoryButton_setDisabled(false);
        View::get()->addToLog(tr("Factory created!"));
    } else if (_factoryLevel == 2) {
        // upgrading existing factory to maximum level
        View::get()->addToLog(tr("Factory upgraded to max. level!"));
    } else {
        // upgrading existing factory (not to maximum level)
        View::get()->addToLog(tr("Factory upgraded!"));
    }

    _towers.push_back(new CTower(2 * _factoryLevel + 1, this));

    _factoryLevel++;

    if (_factoryLevel == 3) {
        // no more upgrades
        View::get()->createOrUpgradeFactoryButton_setDisabled(true);
    } else if (_factoryLevel == 1 && Tree::noPlace()) {
        View::get()->addTreeButton_setDisabled(true);
    }
}

void Factory::destroyOrDowngradeFactory() {
    if (_factoryLevel == 0) {
        qDebug() << "factoryLevel == 0: There is no factory to destroy or downgrade!\n";
        return;
    }
    if (_towers.empty()) {
        qDebug() << "towers empty: Downgrading with no towers!\n";
        return;
    }

    if (_factoryLevel == 1) {
        fadeOut(false);

        View::get()->destroyOrDowngradeFactoryButton_setDisabled(true);
        View::get()->addToLog(tr("Factory destroyed!"));
    } else {
        View::get()->createOrUpgradeFactoryButton_setDisabled(false);
        View::get()->addToLog(tr("Factory downgraded!"));
    }

    _towers.back()->fadeOut(true);
    _towers.pop_back();

    _factoryLevel--;

    if (!Tree::noPlace() && _factoryLevel == 0) {
        View::get()->addTreeButton_setDisabled(false);
    }
}
