#pragma once

#include "View.h"

#include <QMainWindow>
#include <QSplitter>
#include <QGridLayout>
#include <QWidget>

class MainWindow : public QMainWindow {
public:
    MainWindow(QWidget* const parent);

    /**
     * for a right quit
     */
    void closeEvent(QCloseEvent* event) override;
};

