#pragma once

#include "HistoryChart.h"

#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>

#include <QDialog>
#include <QVBoxLayout>

class ChartDialog : public QDialog {
public:
    ChartDialog(QtCharts::QSplineSeries* const series1, QtCharts::QSplineSeries* const series2,
                const int& lastX, const int& biggestY1Value, const int& biggestY2Value, QWidget* const parent);

    HistoryChart* chart() const {return _chart;}

private:
    HistoryChart* _chart;
};

