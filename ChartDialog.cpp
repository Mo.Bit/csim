#include "ChartDialog.h"

#include "View.h"

ChartDialog::ChartDialog(QtCharts::QSplineSeries* const series1, QtCharts::QSplineSeries* const series2,
                         const int& lastX, const int& biggestY1Value, const int& biggestY2Value, QWidget* const parent):
    QDialog(parent)
{
    setWindowTitle(tr("History Chart"));
    setModal(false);

    _chart = new HistoryChart(nullptr, series1, series2, biggestY1Value, biggestY2Value);

    auto chartView = new QtCharts::QChartView(_chart, this);
    chartView->setInteractive(false);

    // Antialiasing
    chartView->setRenderHints(QPainter::Antialiasing);

    auto layout = new QVBoxLayout(this);
    layout->addWidget(chartView);
    setLayout(layout);

    {
        const qreal viewWidth = View::get()->width();
        const qreal viewHeight = View::get()->height();
        setGeometry(viewWidth / 50, viewHeight / 30, viewWidth / 1.35, viewHeight / 1.35);
    }
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    if (lastX >= 0) {
        _chart->resizeX(lastX);
    } else {
        _chart->resizeX(0);
    }

    show();
}
