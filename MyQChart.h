#pragma once

#include <QtCharts/QChart>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>

#include <QFont>
#include <QPen>

class MyQChart : public QtCharts::QChart {
public:
    MyQChart(QGraphicsItem* const parent, QtCharts::QSplineSeries* const series1 = nullptr, QtCharts::QSplineSeries* const series2 = nullptr);

    virtual ~MyQChart() = default;

protected:
    // series
    QtCharts::QSplineSeries* _series1 = nullptr;
    QtCharts::QSplineSeries* _series2 = nullptr;

    // axis
    QtCharts::QValueAxis* _xAxis = nullptr;
    QtCharts::QValueAxis* _y1Axis = nullptr;
    QtCharts::QValueAxis* _y2Axis = nullptr;
};

