# CSim

CSim is an interactive *Climate Simulation* designed mainly for schools.
It is written in C++ with the [Qt framework](https://www.qt.io/).

![Screenshot](screenshot.png)

The project is still work in progress!

___

*You are welcome to contribute to the project in any way!*

`TODO.md` contains some of what can be done. New Ideas are welcome, too!

### How to start working on the project

You need to install [Qt](https://www.qt.io/) with [QtCharts](https://doc.qt.io/qt-5/qtcharts-index.html). The easiest way is to use **Qt Creator**.

#### On Linux:

You need to install the packages `qtcreator` and `qt5-charts` from your distribution repository. Installing `qtcreator` installs the Qt framework.

On Arch-based Linux distributions, for example:

```
pacman -S qtcreator qt5-charts
```

#### On Windows:

Get the [Qt Online Installer](https://www.qt.io/download-open-source) (open source edition). Under the last Qt version, select a compiler (`MinGW` recommended) and `Qt Charts`.
