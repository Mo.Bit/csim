#include "FadableItem.h"

#include "View.h"

FadableItem::FadableItem(QAbstractGraphicsShapeItem* const item, QObject* const parent):
    QObject(parent),
    _item(item),
    _fadeTimer(new QTimer(this))
{}

FadableItem::~FadableItem() {
    _fadeTimer->stop();
}

void FadableItem::fadeOut(const bool& deleteItem) {
    _deleteItemAfterFadeOut = deleteItem;
    fadeOut();
}

void FadableItem::fadeIn() {
    QBrush brush = _item->brush();
    QPen pen = _item->pen();
    QColor brushColor = brush.color();
    QColor penColor = pen.color();

    int newBrushAlpha, newPenAlpha;
    if (!_fadingIn) {
        newBrushAlpha = 51;
        newPenAlpha = 51;
    } else {
        newBrushAlpha = brushColor.alpha() + 51;
        newPenAlpha = penColor.alpha() + 51;
    }

    brushColor.setAlpha(newBrushAlpha);
    penColor.setAlpha(newPenAlpha);
    brush.setColor(brushColor);
    pen.setColor(penColor);
    _item->setBrush(brush);
    _item->setPen(pen);

    if (!_fadingIn) {
        View::get()->addToScene(_item);
        _fadeConnection = connect(_fadeTimer, SIGNAL(timeout()),
                                  this, SLOT(fadeIn()));
        _fadeTimer->start(75);
        _fadingIn = true;
    } else if (newBrushAlpha == 255) {
        _fadingIn = false;
        _fadeTimer->stop();
        disconnect(_fadeConnection);
    }
}

void FadableItem::fadeOut() {
    if (_fadingIn) {
        _fadingIn = false;
        _fadeTimer->stop();
        disconnect(_fadeConnection);
    }

    QBrush brush = _item->brush();
    QPen pen = _item->pen();
    QColor brushColor = brush.color();
    QColor penColor = pen.color();

    int newBrushAlpha, newPenAlpha;
    if (!_fadingOut) {
        newBrushAlpha = 204;
        newPenAlpha = 204;
    } else {
        newBrushAlpha = brushColor.alpha() - 51;
        newPenAlpha = penColor.alpha() - 51;
    }

    brushColor.setAlpha(newBrushAlpha);
    penColor.setAlpha(newPenAlpha);
    brush.setColor(brushColor);
    pen.setColor(penColor);
    _item->setBrush(brush);
    _item->setPen(pen);

    if (!_fadingOut) {
        _fadeConnection = connect(_fadeTimer, SIGNAL(timeout()),
                                  this, SLOT(fadeOut()));
        _fadeTimer->start(75);
        _fadingOut = true;
    } else if (newBrushAlpha == 0) {
        _fadingOut = false;
        _fadeTimer->stop();
        disconnect(_fadeConnection);
        View::get()->removeFromScene(_item);
        if (_deleteItemAfterFadeOut) {
            delete _item;
            return;
        }
    }
}
