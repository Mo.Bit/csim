#pragma once

#include "Dim2Vector.h"

#include <QRandomGenerator>

struct MovementUnitVector : public Dim2Vector {
public:
    MovementUnitVector() = default;

    ~MovementUnitVector() = default;

    /**
     * @brief MovementUnitVector makes an *unit* vector from two points
     * @param x1 coordinate of first point
     * @param y1 coordinate of first point
     * @param x2 coordinate of second point
     * @param y2 coordinate of second point
     */
    MovementUnitVector(const qreal& x1, const qreal& y1, const qreal& x2, const qreal& y2);

    MovementUnitVector(const qreal& x, const qreal& y);

    /**
     * @brief generateRandomUnitVector generates a random unit vector
     */
    void generateRandomUnitVector();
};
