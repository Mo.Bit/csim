#pragma once

#include "FadableItem.h"
#include "CTower.h"

#include <QGraphicsRectItem>
#include <QGraphicsPolygonItem>
#include <QPolygonF>
#include <QPointF>

class House : public FadableItem, public QGraphicsRectItem {
    Q_OBJECT
public:
    House(const qreal& x, const qreal& y, QObject* const parent, const bool &oneOfFirstHouses = false);

private:
    class BodyOutline : public FadableItem, public QGraphicsRectItem {
    public:
        BodyOutline(const qreal& x, const qreal& y, QObject* const parent);
    };

    class Roof : public FadableItem, public QGraphicsPolygonItem {
    public:
        Roof(const qreal& x, const qreal& y, QObject* const parent);
    };

    House::BodyOutline* _bodyOutline = nullptr;
    House::Roof* _roof = nullptr;

    CTower* _tower = nullptr;
};

