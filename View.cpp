#include "View.h"

#include "Particle.h"
#include "Light.h"
#include "Ice.h"
#include "Tree.h"
#include "Factory.h"
#include "RightSideChart.h"
#include "Water.h"
#include "House.h"
#include "Airplane.h"

#include <cmath>

View* View::_instance = nullptr;

View::View(QWidget* const rightSideWidget, QWidget* const parent):
    QGraphicsView(parent),
    _rightSideWidget(rightSideWidget),
    _scene(new QGraphicsScene(this))
{
    View::_instance = this;

    setInteractive(false);

    setMinimumSize(400,300); // minimum size

    setBackgroundBrush(QBrush(Qt::white)); // background color
    setCacheMode(QGraphicsView::CacheBackground);

    _rightSideGridLayout = new QGridLayout(rightSideWidget);
    rightSideWidget->setLayout(_rightSideGridLayout);

    MyTimer::setup(); // not paused

    setRenderHints(QPainter::Antialiasing); // Antialiasing

    _initTimer = new QTimer(this);
    _initTimer->setSingleShot(true);
    connect(_initTimer, SIGNAL(timeout()),
            this, SLOT(resumeInit()));
    _initTimer->start(3000);

//    repaint strategy
//    setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate); // minimal is the best
//    _scene->setItemIndexMethod(QGraphicsScene::NoIndex); // slows down!
}

View::~View() {
    View::_instance = nullptr;
    delete _factory;
}

void View::deleteIce() {
    delete _ice;
    _ice = nullptr;
}

void View::setNewHousesBuiltToTrue() {
    _newHousesBuilt = true;
    View::get()->addToLog(tr("New houses built!"));
}

void View::addToLog(const QString &txt) {
    _log->insertItem(0, txt);
    _log->setCurrentRow(0);
}

void View::addCButton_setDisabled(const bool &b) {
    _addCButton->setDisabled(b);
}

void View::addTreeButton_setDisabled(const bool &b) {
    _addTreeButton->setDisabled(b);
}

void View::createOrUpgradeFactoryButton_setDisabled(const bool &b) {
    _createOrUpgradeFactoryButton->setDisabled(b);
}

void View::destroyOrDowngradeFactoryButton_setDisabled(const bool &b) {
    _destroyOrDowngradeFactoryButton->setDisabled(b);
}

void View::createSecondHouses() {
    for (MyTimer* timer : _firstHousesTimers ) {
        timer->stop();
    }

    new House(View::get()->widthPart() * 3, View::get()->heightPart() * 8.7, View::get());
    new House(View::get()->widthPart() * 3.5, View::get()->heightPart() * 8.7, View::get());
}

void View::setFirstHousesTimers(MyTimer * const timer) {
    if (_firstHousesTimers[0] == nullptr) {
        _firstHousesTimers[0] = timer;
    } else {
        _firstHousesTimers[1] = timer;
    }
}

void View::resumeInit() {
    delete _initTimer;
    _initTimer = nullptr;

    // set scene
    _scene->setSceneRect(QRectF(0, 0,
                                width(), height()));
    setScene(_scene);
    fitInView(sceneRect());

    // parts
    _widthPart = scene()->width() / 10;
    _heightPart = scene()->height() / 10;

    _rightSideChart = new RightSideChart(nullptr); // right side chart

    _factory = new Factory(this); // init Factory

    create_Buttons_Log_Layout(); // buttons, log and layout

    createSun(); // sun

    createGround(); // ground

    createFirstHouses(); // houses

    Particle::setupAndAdd(); // create particles on start

    // create ice and water
    _waterHeightAtStart = _heightPart * 9.7;
    _ice = new Ice(this);
    _water = new Water(this);

    // create sun light and set timer
    Light::createSunLight();
    _createSunLightTimer = new MyTimer(this);
    QObject::connect(_createSunLightTimer, &QTimer::timeout, Light::createSunLight);
    _createSunLightTimer->customStart(10000);

    Tree::setupAndAdd(); // create trees

    createParticlesLegend(); // Legend for particles

    new Airplane();
}

void View::exitOrRestartFun() {
    QMessageBox msgBox;
    msgBox.setText(tr("Do you want to close or restart the simulation?"));
    QPushButton* cancelButton = msgBox.addButton(tr("Cancel"), QMessageBox::RejectRole);
    QPushButton* exitButton = msgBox.addButton(tr("Exit"), QMessageBox::DestructiveRole);
    exitButton->setIcon(QIcon(":icons/close-line.svg"));
    QPushButton* restartButton = msgBox.addButton(tr("Restart"), QMessageBox::ResetRole);
    restartButton->setIcon(QIcon(":/icons/restart-line.svg"));
    msgBox.setDefaultButton(cancelButton);
    msgBox.exec();

    if (msgBox.clickedButton() == exitButton) {
        QApplication::exit(0);
    } else if (msgBox.clickedButton() == restartButton) {
        QApplication::exit(42);
    }
}

void View::pauseOrResumeFun() {
    if (_pauseOrResumeButton->text() == tr("&Pause")) {
        _pauseOrResumeButton->setText(tr("&Resume"));
        _pauseOrResumeButton->setIcon(QIcon(":/icons/play-line.svg"));

        MyTimer::setPaused(true);
    } else {
        _pauseOrResumeButton->setText(tr("&Pause"));
        _pauseOrResumeButton->setIcon(QIcon(":/icons/pause-line.svg"));

        MyTimer::setPaused(false);
    }
}

void View::speedUpFun() {
    const bool b = _speedUpButton->isChecked();

    MyTimer::setBoosted(b);

    if (b) {
        _speedUpButton->setIcon(QIcon(":/icons/speed-fill.svg"));
    } else {
        _speedUpButton->setIcon(QIcon(":/icons/speed-line.svg"));
    }
}

void View::create_Buttons_Log_Layout() {
    _addTreeButton = new MyQPushButton(QIcon(":/icons/add-line.svg"), tr("&Tree"), this);
    QObject::connect(_addTreeButton, &QPushButton::clicked, Tree::addTree);
    _addTreeButton->setToolTip(tr("Add a Tree"));

    _addCButton = new MyQPushButton(QIcon(":/icons/add-line.svg"), tr("&CO2"), this);
    QObject::connect(_addCButton, &QPushButton::clicked, Particle::addP);
    _addCButton->setToolTip(tr("Add CO2 manually"));

    _createOrUpgradeFactoryButton = new MyQPushButton(QIcon(":/icons/add-line.svg"), tr("Factory"), this);
    QObject::connect(_createOrUpgradeFactoryButton, SIGNAL(clicked()),
                     _factory, SLOT(createOrUpgradeFactory()));
    _createOrUpgradeFactoryButton->setToolTip(tr("Create or upgrade the factory"));

    _destroyOrDowngradeFactoryButton = new MyQPushButton(QIcon(":/icons/subtract-line.svg"), tr("Factory"), this);
    QObject::connect(_destroyOrDowngradeFactoryButton, SIGNAL(clicked()),
                     _factory, SLOT(destroyOrDowngradeFactory()));
    _destroyOrDowngradeFactoryButton->setToolTip(tr("Destroy or downgrade the factory"));
    _destroyOrDowngradeFactoryButton->setDisabled(true);

    // log
    _log = new QListWidget(this);
    _log->setWordWrap(true);
    _log->setAlternatingRowColors(true);
    _log->setToolTip(tr("Log"));

    // buttons
    // icons from https://remixicon.com/
    _pauseOrResumeButton = new MyQPushButton(QIcon(":/icons/pause-line.svg"), tr("&Pause"), this);
    QObject::connect(_pauseOrResumeButton, SIGNAL(clicked()),
                     this, SLOT(pauseOrResumeFun()));

    _speedUpButton = new MyQPushButton(QIcon(":/icons/speed-line.svg"), tr("&Speed up"), this);
    _speedUpButton->setCheckable(true);
    QObject::connect(_speedUpButton, SIGNAL(clicked()),
                     this, SLOT(speedUpFun()));

    auto settingsButton = new MyQPushButton(QIcon(":/icons/settings-5-line.svg"), tr("Settings"), this);

    auto exitOrRestartButton = new MyQPushButton(QIcon(":/icons/close-line.svg"), tr("Exit/Restart"), this);
    QObject::connect(exitOrRestartButton, SIGNAL(clicked()),
                     this, SLOT(exitOrRestartFun()));

    //###//

    _rightSideGridLayout->setRowStretch(1, 1);
    _rightSideGridLayout->addWidget(_addTreeButton, 1,0);
    _rightSideGridLayout->addWidget(_addCButton, 1,1);

    _rightSideGridLayout->setRowStretch(2, 1);
    _rightSideGridLayout->addWidget(_createOrUpgradeFactoryButton, 2,0);
    _rightSideGridLayout->addWidget(_destroyOrDowngradeFactoryButton, 2,1);

    _rightSideGridLayout->setRowStretch(3, 3);
    _rightSideGridLayout->addWidget(_log, 3,0, 1,0);

    _rightSideGridLayout->setRowStretch(4, 1);
    _rightSideGridLayout->addWidget(_pauseOrResumeButton, 4,0);
    _rightSideGridLayout->addWidget(_speedUpButton, 4,1);

    _rightSideGridLayout->setRowStretch(5, 1);
    _rightSideGridLayout->addWidget(settingsButton, 5,0);
    _rightSideGridLayout->addWidget(exitOrRestartButton, 5,1);
}

void View::createSun() {
    auto sun = new QGraphicsEllipseItem(-_heightPart+_widthPart*5, -_heightPart,
                                        2*_heightPart, 2*_heightPart);
    sun->setBrush(QBrush(Qt::yellow));
    sun->setPen(QPen(Qt::yellow));
    _scene->addItem(sun);
}

void View::createGround() {
    QPolygonF groundPolygon;
    groundPolygon << QPointF(0, height())
                  << QPointF(0, _heightPart*9)
                  << QPointF(_widthPart*5, _heightPart*9)
                  << QPointF(_widthPart*5.5, _heightPart*9.3)
                  << QPointF(_widthPart*6.5, _heightPart*9.3)
                  << QPointF(_widthPart*7, height());
    auto ground = new QGraphicsPolygonItem(groundPolygon);
    ground->setBrush(QBrush(Qt::darkGray));
    ground->setZValue(3);
    _scene->addItem(ground);
}

void View::createFirstHouses() {
    new House(_widthPart * 6, _heightPart * 9, this, true);
    new House(_widthPart * 5.5, _heightPart * 9, this, true);
}

void View::createParticlesLegend() {
    const auto cText = new QGraphicsSimpleTextItem(tr("CO2"));
    const auto waterText = new QGraphicsSimpleTextItem(tr("H2O"));
    const auto othersText = new QGraphicsSimpleTextItem(tr("Others"));

    QFont textFont = cText->font();
    textFont.setPointSize(View::get()->widthPart() * View::get()->heightPart() * 0.001);
    cText->setFont(textFont);
    waterText->setFont(textFont);
    othersText->setFont(textFont);

    waterText->setBrush(QBrush(Qt::blue));
    othersText->setBrush(QBrush(Qt::darkGray));

    const qreal textXPos = View::get()->widthPart() * 0.3;
    cText->setPos(textXPos, 0);
    waterText->setPos(textXPos, View::get()->heightPart() * 0.2);
    othersText->setPos(textXPos, View::get()->heightPart() * 0.4);

    _scene->addItem(cText);
    _scene->addItem(waterText);
    _scene->addItem(othersText);

    const qreal textPRadius = Particle::pRadius() * 2;

    const auto cParticleToText = new QGraphicsEllipseItem(0,0,
                                                          textPRadius * 2, textPRadius * 2);
    const auto waterParticleToText = new QGraphicsEllipseItem(0,0,
                                                              textPRadius * 2, textPRadius * 2);
    const auto othersParticle1ToText = new QGraphicsEllipseItem(0,0,
                                                                textPRadius * 2, textPRadius * 2);
    const auto othersParticle2ToText = new QGraphicsEllipseItem(0,0,
                                                                textPRadius * 2, textPRadius * 2);

    cParticleToText->setBrush(QBrush(Qt::black));
    waterParticleToText->setBrush(QBrush(Qt::blue));
    othersParticle1ToText->setBrush(QBrush(Qt::red));
    othersParticle2ToText->setBrush(QBrush(Qt::gray));

    cParticleToText->setPos(View::get()->widthPart() * 0.1 - textPRadius, View::get()->heightPart() * 0.07);
    waterParticleToText->setPos(View::get()->widthPart() * 0.1 - textPRadius, View::get()->heightPart() * 0.27);
    othersParticle1ToText->setPos(View::get()->widthPart() * 0.1 - textPRadius, View::get()->heightPart() * 0.47);
    othersParticle2ToText->setPos(View::get()->widthPart() * 0.2 - textPRadius, View::get()->heightPart() * 0.47);

    _scene->addItem(cParticleToText);
    _scene->addItem(waterParticleToText);
    _scene->addItem(othersParticle1ToText);
    _scene->addItem(othersParticle2ToText);

    const auto legendRect = new QGraphicsRectItem(0,0,
                                                  View::get()->widthPart() * 0.9, View::get()->heightPart() * 0.8);
    _scene->addItem(legendRect);
}

void View::resizeEvent(QResizeEvent* event) {
    event->accept();
    fitInView(sceneRect());
}
