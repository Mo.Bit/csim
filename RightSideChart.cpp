#include "RightSideChart.h"

#include "View.h"
#include "ChartDialog.h"

RightSideChart::RightSideChart(QGraphicsItem* const parent):
    MyQChart(parent),
    _dialogSeries1(new QtCharts::QSplineSeries(this)),
    _dialogSeries2(new QtCharts::QSplineSeries(this)),
    _chartView(new QtCharts::QChartView(this, View::get()->rightSideWidget()))
{
    setMinimumSize(250,200); // minimum size
    legend()->hide(); // hide legend
    setMargins(QMargins(0,0,0,0)); // remove margins

    View::get()->rightSideGridLayout()->setRowStretch(0, 3);
    View::get()->rightSideGridLayout()->addWidget(_chartView, 0,0, 1,0);

    _chartView->setRenderHints(QPainter::Antialiasing); // Antialiasing

    setToolTip(toolTip() + ".\n" +
                           tr("Click on it to view a bigger chart with the whole history!"));

    setCursor(Qt::PointingHandCursor);
}

void RightSideChart::update(const int &y1, const int &y2)
{
    _x++; // encrease x
    if (_x == -1) {
        // to not update on the first time
        return;
    }

    // append new values
    _series1->append(_x, y1);
    _series2->append(_x, y2);

    _dialogSeries1->append(_x, y1);
    _dialogSeries2->append(_x, y2);
    if (_chartDialog != nullptr) {
        _chartDialog->chart()->resizeX(_x);
    }

    checkYRange(y1, y2);
    if (_x + 1 >= _xAxis->tickCount()) {
        scroll(plotArea().width() / (_xAxis->tickCount() - 1), 0); // scroll the right side chart
    }

}

void RightSideChart::checkYRange(const int& y1, const int& y2) {
    if (y1 > _y1UpRange) {
        // set bigger y1 range
        _y1UpRange += int(y1/_y1UpRange) * 15;
        _y1Axis->setMax(_y1UpRange);

        if (y1 > _biggestY1Value) {
            _biggestY1Value = _y1UpRange;
            if (_chartDialog != nullptr) {
                _chartDialog->chart()->setBiggerY1Range(_biggestY1Value);
            }
        }
    } else if (y1 < _y1UpRange - 100) {
        // set smaller y1 range
        _y1UpRange -= 50;
        _y1Axis->setMax(_y1UpRange);
    }

    if (y2 > _y2UpRange) {
        // set bigger y2 range
        _y2UpRange += int(y2/_y2UpRange) * 50;
        _y2Axis->setMax(_y2UpRange);

        if (y2 > _biggestY2Value) {
            _biggestY2Value = _y2UpRange;
            if (_chartDialog != nullptr) {
                _chartDialog->chart()->setBiggerY2Range(_biggestY2Value);
            }
        }
    } else if (y2 < _y2UpRange - 100) {
        // set smaller y2 range
        _y2UpRange -= 50;
        _y2Axis->setMax(_y2UpRange);
    }
}

void RightSideChart::mousePressEvent(QGraphicsSceneMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        event->accept();

        if (_chartDialog == nullptr) {
            _chartDialog = new ChartDialog(_dialogSeries1, _dialogSeries2, _x, _biggestY1Value, _biggestY2Value, View::get());
        } else {
            _chartDialog->show();
        }
    }
}
