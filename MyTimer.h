#pragma once

#include <QTimer>
#include <QObject>

class MyTimer : public QTimer {
public:
    MyTimer(QObject* const parent);

    ~MyTimer();

    static void setup();

    static void setPaused(const bool& b);

    static void setBoosted(const bool& b);

    void customStart(const int& msec = -1);

    void customSetInterval(const int& msec);

private:
    static bool _paused;

    static bool _boosed;

    static std::list<MyTimer*> _allTimers;

    int _interval = 0;

    using QTimer::start;
    using QTimer::setInterval;
};
