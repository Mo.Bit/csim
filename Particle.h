#pragma once

#include "Tree.h"
#include "MovementUnitVector.h"
#include "MyTimer.h"
#include "FadableItem.h"

#include <list>
#include <array>

#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <QMetaObject>
#include <QColor>
#include <QBrush>
#include <QRectF>
#include <QPointF>
#include <QRandomGenerator>

class Particle : public FadableItem, public QGraphicsEllipseItem {
    Q_OBJECT
public:
    static void setupAndAdd();

    /**
     * @return pointer to last element in cParticles after removing it from cParticles or nullptr if cParticles is empty
     */
    static Particle* takeOneCParticle();

    static int numCParticles() {return Particle::_cParticles.size();}
    static int pRadius() {return Particle::_pRadius;}
    static int manuallyAddCLimit() {return Particle::_manuallyAddCLimit;}
    static qreal particleUpperYLimit() {return Particle::_particleUpperYLimit;}
    static qreal particleLowerYLimit() {return Particle::_particleLowerYLimit;}

    /**
     * @brief addP adds Particles
     * @param inputNum number of particles to add
     */
    static void addP(const unsigned int &inputNum = 0);

    /**
     * @brief produce creates a CO2 particle from down (not in the atmosphere)
     * @param x x-coordinate
     * @param y y-coordiante
     * @return pointer to the created particle
     */
    static Particle* produce(const qreal &x, const qreal &y);

    /**
     * @brief toMoveToTree directs to moveToTree() which moves the particle to tree
     * @param inDistanceToTree the distance to the tree
     * @param vectToTree unit vector to the tree
     * @param inAbsorbTree the absorbing tree
     */
    void toMoveToTree(const qreal &distanceToTree, const MovementUnitVector &vectToTree,
                      Tree* const inAbsorbTree);

public slots:
    /**
     * @brief move moves the particle in the atmosphere
     */
    void move();

    /**
     * @brief moveUp moves the particle up to the atmosphere
     */
    void moveUp();

private slots:
    /**
     * @brief moveToTree moves the CO2 particle to the tree
     */
    void moveToTree();

private:
    static const int _moveToTreeStep = 20; // steps for the movement to a tree
    static const std::array<QColor, 4> _pColors; // particles corlors

    static qreal _diameter;
    static qreal _pRadius; // particle radius
    static qreal _particleUpperYLimit;
    static qreal _particleLowerYLimit;

    MovementUnitVector _vect; // movement unit vector

    // timer and connection
    MyTimer* _moveTimer;
    static const unsigned int _timerInterval = 110;
    QMetaObject::Connection _moveConnection;

    // for the movement to a tree
    qreal _disatanceToTree;
    Tree* _absorbTree  = nullptr;
    unsigned int _absorbTreeIndex;

    static const int _manuallyAddCLimit = 200;

    static std::list<Particle*> _cParticles; // all CO2 particles

    /**
     * @brief Particle sets movement unit vector, adds to scene and starts movement
     * @param x coordinate of the center
     * @param y coordinate of the center
     * @param color of the particle
     * @param producedDown wether the particle is produced down or not
     */
    Particle(qreal x, qreal y, const QColor &color = Qt::black, const bool &producedDown = false);

    /**
     * @brief toMove makes connectino to move()
     */
    void toMove();
};
