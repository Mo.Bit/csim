#include "MyQChart.h"

#include <QDebug>

MyQChart::MyQChart(QGraphicsItem* const parent, QtCharts::QSplineSeries* const series1, QtCharts::QSplineSeries* const series2):
    QChart(parent),

    _xAxis(new QtCharts::QValueAxis(this)),
    _y1Axis(new QtCharts::QValueAxis(this)),
    _y2Axis(new QtCharts::QValueAxis(this))
{
    // series
    if (series1 == nullptr || series2 == nullptr) {
        _series1 = new QtCharts::QSplineSeries(this);
        _series2 = new QtCharts::QSplineSeries(this);
    } else {
        _series1 = series1;
        _series2 = series2;
    }

    _xAxis->setTitleText("Time");

    {
        // color and width
        QPen pen(Qt::red);
        pen.setWidth(3);
        _series1->setPen(pen);
        _y1Axis->setLabelsColor(pen.color());
    }
    _y1Axis->setTitleText("Reflections (Greenhouse effect)"); // title

    {
        // color and width
        QPen pen(Qt::blue);
        pen.setWidth(3);
        _series2->setPen(pen);
        _y2Axis->setLabelsColor(pen.color());
    }
    _y2Axis->setTitleText("CO2 particles"); // title

    // add series
    addSeries(_series1);
    addSeries(_series2);

    // add axis
    addAxis(_xAxis, Qt::AlignBottom);
    addAxis(_y1Axis, Qt::AlignLeft);
    addAxis(_y2Axis, Qt::AlignRight);

    // set axis for series 1
    _series1->attachAxis(_xAxis); // x
    _series1->attachAxis(_y1Axis); // y

    // set axis for series 2
    _series2->attachAxis(_xAxis); // x
    _series2->attachAxis(_y2Axis); // y

    // set axis labels to only show integers
    _xAxis->setLabelFormat("%i"); // i: integer
    _y1Axis->setLabelFormat("%i");
    _y2Axis->setLabelFormat("%i");

    {
        // set bigger font size for all labels/titles
        QFont font = _xAxis->labelsFont();

        font.setPointSize(6);
        _xAxis->setLabelsFont(font);

        font.setPointSize(8);
        _y1Axis->setLabelsFont(font);
        _y2Axis->setLabelsFont(font);

        _y1Axis->setTitleFont(font);
        _y2Axis->setTitleFont(font);
        _xAxis->setTitleFont(font);
    }

    _xAxis->setTickCount(6); // tick count

    // set ranges
    _xAxis->setRange(0, 5);
    _y1Axis->setRange(0, 50);
    _y2Axis->setRange(0, 50);

    setAnimationOptions(QtCharts::QChart::AllAnimations);

    setToolTip(tr("Live chart representing change of number of\n"
                  "CO2 molecules in the atmosphere\n"
                  "and light reflections over time"));
}
