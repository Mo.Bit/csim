#include "Light.h"

#include "View.h"
#include "Particle.h"
#include "Ice.h"
#include "MovementUnitVector.h"
#include "RightSideChart.h"
#include "CTower.h"

#include <cmath>

unsigned int Light::_numReflectedDown = 0;
const qreal Light::_lightStep = 10;

Light::Light(const qreal &num, const qreal &all, const LightMode &inLightMode, const bool &fromSun,
             const qreal &x1, const qreal &y1,
             const qreal &x2, const qreal &y2):
    _lightTimer(new MyTimer(this)),
    _lightMode(inLightMode),
    _collisionLine(new QGraphicsLineItem())
{
    // color and pen style based on light mode (wave)
    QColor color;
    Qt::PenStyle penStyle;
    if (inLightMode == ShortWave) {
        color = Qt::blue;
        penStyle = Qt::DotLine;
    } else {
        // Longwave
        color = Qt::red;
        penStyle = Qt::DashLine;
    }

    // movement unit vector
    qreal x, y;
    if (!fromSun) {
        // reflected
        _vect = MovementUnitVector(x1,y1, x2,y2);
        x = x1;
        y = y1;
    } else {
        // from sun
        const qreal phi = (num / all) * M_PI;
        x = View::get()->heightPart() * cos(phi) + View::get()->widthPart()*5;
        y = View::get()->heightPart() * sin(phi);
        _vect = MovementUnitVector(View::get()->widthPart()*5,0, x,y);
    }

    setLine(x, y,
            x, y);
    {
        const qreal penWidth = View::get()->widthPart() * 0.023;
        setPen(QPen(color, penWidth, penStyle));
    }

    setZValue(-1); // Z-Value
    View::get()->addToScene(this); // add to scene

    // collision line
    _collisionLine->setVisible(false);
    _collisionLine->setPen(pen());

    develop(Grow); // start growing
}

Light::~Light() {
    delete _collisionLine;
}

void Light::move() {
    QLineF l = line();

    if (l.y1() < 0 || l.y1() > View::get()->scene()->height()
        || (l.x1() < 0 && _vect.x() < 0) || (l.x1() > View::get()->scene()->width() && _vect.x() > 0)) {
        // delete
        delete this;
        return;
    }

    const QPointF point2 = l.p2();
    const qreal furtherPointX = point2.x() + _vect.x() * Light::_lightStep;
    if ((furtherPointX+1 >= View::get()->scene()->width() && _vect.x() > 0) // +1 as correction for no reflectoin on rightSideRect
        || (furtherPointX <= 0 && _vect.x() < 0)) {
        // out of range
        new Light(0,0, _lightMode, false, l.x2(),l.y2(), l.x1(),2*l.y2()-l.y1());

        _lightTimer->stop();
        disconnect(_lightConnection);
        develop(Shrink);
        return;
    }

    if (l.y2() > Particle::particleUpperYLimit()) {
        // reflection
        _collisionLine->setLine(QLineF(point2, QPointF(furtherPointX, point2.y() + _vect.y() * Light::_lightStep)));
        View::get()->addToScene(_collisionLine);
        const QList<QGraphicsItem*> collidingItemsList = _collisionLine->collidingItems();
        View::get()->removeFromScene(_collisionLine);

        for (auto i : collidingItemsList) {
            const auto& currentCollidingItem = *i;

            bool shrink = true;
            bool notIce = true;
            if (l.y2() < Particle::particleLowerYLimit()
                    && i->type() == QGraphicsEllipseItem::Type
                    && _lightMode == LongWave) {

            }
            else if ((i->type() == QGraphicsRectItem::Type
                      && typeid(currentCollidingItem) != typeid(CTower))
                     || i->type() == QGraphicsPolygonItem::Type) {
                if (typeid(currentCollidingItem) == typeid(Ice)) {
                    notIce = false;
                }

                if (_lightMode == LongWave) {
                    Light::_numReflectedDown++;
                }
            } else {
                shrink = false;
            }

            if (shrink) {
                if (notIce) {
                    new Light(0,0, LongWave, false, l.x2(),l.y2(), 2 * l.x2() - l.x1(),l.y1());
                } else {
                    new Light(0,0, _lightMode, false, l.x2(),l.y2(), l.x1(),l.y1());
                }
                _lightTimer->stop();
                disconnect(_lightConnection);
                develop(Shrink);
                return;
            }
        }
    }

    l.translate(_vect.x() * Light::_lightStep, _vect.y() * Light::_lightStep);
    setLine(l);
}

void Light::grow() {
    QLineF l = line();
    if (_devLevel <= _numGrowParts) {
        // still growing
        l.setP2(QPointF(_growP1X + _vect.x() * _devLevel * Light::_lightStep, _growP1Y + _vect.y() * _devLevel * Light::_lightStep));
        setLine(l);

        _devLevel++;
    } else {
        // finished growing -> move()
        l.setP2(QPointF(_growP1X + _vect.x() * View::get()->heightPart(), _growP1Y + _vect.y() * View::get()->heightPart()));
        setLine(l);

        _lightTimer->stop();
        disconnect(_lightConnection);

        _lightConnection = connect(_lightTimer, SIGNAL(timeout()),
                                   this, SLOT(move()));
        _lightTimer->customStart(_lightTimerInterval);
    }
}

void Light::shrink() {
    if (_devLevel <= _numGrowParts) {
        // still shrinking
        QLineF l = line();
        l.setP1(QPointF(_shrinkP1X + _vect.x() * _devLevel * Light::_lightStep, _shrinkP1Y + _vect.y() * _devLevel * Light::_lightStep));
        setLine(l);

        _devLevel++;
    } else {
        // finished shrinking -> delete light
        _lightTimer->stop();

        delete this;
        return;
    }
}

void Light::createSunLight() {
    const unsigned int pRRate = 100 * Light::_numReflectedDown / (Light::_lightsNum/2); // particle reflection rate

    if (!View::get()->iceMelted() && pRRate > 400) {
        View::get()->ice()->startMelting();
        View::get()->addToLog(tr("Ice started melting!"));
    }

    const int numCParticles = Particle::numCParticles();
    View::get()->chart()->update(Light::_numReflectedDown, numCParticles);

    Light::_numReflectedDown = 0;
    for (unsigned int i = Light::_lightsNum/4, n = Light::_lightsNum * 3/4; i < n; i++) {
        new Light(i+1, Light::_lightsNum+1, ShortWave, true);
    }

    if (numCParticles < Particle::manuallyAddCLimit()) {
        // number of CO2 particles under limit -> able to add more manually
        View::get()->addCButton_setDisabled(false);
    }
}

void Light::develop(const DevelopmentMode &dev) {
    _devLevel = 1; // 1 -> numGrowParts
    const QPointF p = line().p1();
    if (dev == Grow) {
        // to grow()
        _growP1X = p.x();
        _growP1Y = p.y();
        _lightConnection = connect(_lightTimer, SIGNAL(timeout()),
                                   this, SLOT(grow()));
    } else {
        // to shrink()
        _shrinkP1X = p.x();
        _shrinkP1Y = p.y();
        _lightConnection = connect(_lightTimer, SIGNAL(timeout()),
                                   this, SLOT(shrink()));
    }
    _lightTimer->customStart(_lightTimerInterval);
}
