#include "CTower.h"

#include "View.h"
#include "Particle.h"

CTower::CTower(const qreal &factor, QObject* const parent):
    FadableItem(this, parent),
    _towerWidth(View::get()->widthPart() / 7)
{
    setRect(View::get()->widthPart() * 4  + factor * _towerWidth, View::get()->heightPart() * 6.5,
            _towerWidth, View::get()->heightPart() * 1.3);

    continueConstruction();
}

CTower::CTower(const qreal &x, const qreal &y,
               const qreal &width, const qreal &height,
               const bool &sendfirstHouseTimer, const qreal &zValue, QObject* const parent,
               const int &timerInterval):
    FadableItem(this, parent),
    _towerWidth(width)
{
    setRect(x, y,
            _towerWidth, height);

    continueConstruction(timerInterval, zValue, sendfirstHouseTimer);
}

void CTower::produceC() {
    const QRectF r = rect();
    Particle::produce(r.x() + 0.5 * _towerWidth, r.y());
}

void CTower::continueConstruction(const int &timerInterval, const qreal &zValue,
                                  const bool &sendFirstHouseTimer) {
    setBrush(QBrush(Qt::gray));
    setZValue(zValue);

    _towerTimer = new MyTimer(this);
    if (sendFirstHouseTimer) {
        View::get()->setFirstHousesTimers(_towerTimer);
    }

    _produceCConnection = connect(_towerTimer, SIGNAL(timeout()),
                                  this, SLOT(produceC()));

    fadeIn();
    _towerTimer->customStart(timerInterval);
}
