#include "Airplane.h"

#include "Particle.h"
#include "View.h"

Airplane::Airplane():
    _toMoveTimer(new MyTimer(this)),
    _moveTimer(new MyTimer(this))
{
    _toMoveTimer->setSingleShot(true);
    _toMoveTimer->customSetInterval(40000);
    _moveTimer->customSetInterval(150);

    connect(_toMoveTimer, SIGNAL(timeout()),
            this, SLOT(toMove()));
    connect(_moveTimer, SIGNAL(timeout()),
            this, SLOT(move()));

    {
        const QPixmap tmpPixmap = QPixmap(":/img/airplane.png");
        _width = View::get()->widthPart();
        _height = View::get()->heightPart();
        setPixmap(tmpPixmap.scaled(_width, _height, Qt::KeepAspectRatio));
    }
    _firstPosX = - _width;
    _firstPosY = Particle::particleUpperYLimit() - _height * 0.25;
    hide();
    View::get()->addToScene(this);

    _toMoveTimer->customStart();
}

void Airplane::toMove() {
    setPos(_firstPosX, _firstPosY);
    show();
    _moveTimer->customStart();
}

void Airplane::move() {
    static int produceCCountDown = 15;
    const qreal xPos = pos().x();
    if (xPos >= View::get()->scene()->width()) {
        _moveTimer->stop();
        hide();
        _toMoveTimer->customStart();
        return;
    }

    if (produceCCountDown == 0) {
        produceCCountDown = 15;
        Particle::produce(xPos + _width * 0.25, pos().y() + _height * 0.5);
    }

    moveBy(_width * 0.05, 0);
    produceCCountDown--;
}
