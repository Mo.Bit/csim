#pragma once

#include "FadableItem.h"
#include "MyTimer.h"
#include "CTower.h"

#include <vector>

#include <QGraphicsRectItem>
#include <QRectF>
#include <QBrush>

class Factory : public FadableItem, public QGraphicsRectItem {
    Q_OBJECT
public:
    Factory(QObject* const parent);

    unsigned int factoryLevel() const {return _factoryLevel;}

public slots:
    void createOrUpgradeFactory();

    void destroyOrDowngradeFactory();

private:
    unsigned int _factoryLevel = 0;

    std::vector<CTower*> _towers;
};
