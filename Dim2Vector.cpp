#include "Dim2Vector.h"

#include <cmath>

Dim2Vector::Dim2Vector(const qreal& x1, const qreal& y1, const qreal& x2, const qreal& y2):
    _x(x2 - x1),
    _y(y2 - y1)
{}

qreal Dim2Vector::length() const {
    return sqrt(pow(_x, 2) + pow(_y, 2));
}

void Dim2Vector::invertX() {
    _x = - _x;
}

void Dim2Vector::invertY() {
    _y = - _y;
}

void Dim2Vector::setX(const qreal& x) {
    _x = x;
}

void Dim2Vector::setY(const qreal& y) {
    _y = y;
}

void Dim2Vector::makeUnit() {
    const qreal len = length();
    _x /= len;
    _y /= len;
}
