#include "MainWindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow* mainWindow = nullptr;
    int exitValue = 42;
    while (exitValue == 42) {
        mainWindow = new MainWindow(nullptr);
        exitValue = app.exec();

        delete mainWindow;
    }

    return 0;
}
