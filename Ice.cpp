#include "Ice.h"

#include "View.h"
#include "Water.h"

Ice::Ice(QObject* const parent):
    QObject(parent),
    _meltTimer(new MyTimer(this))
{
    {
        QPolygonF icePolygon;
        icePolygon << QPointF(View::get()->widthPart()*7, View::get()->waterHeightAtStart())
                   << QPointF(View::get()->widthPart()*8, View::get()->heightPart()*7)
                   << QPointF(View::get()->widthPart()*8.5, 0.5 * View::get()->waterHeightAtStart() + 3.5 * View::get()->heightPart())
                   << QPointF(View::get()->widthPart()*9, View::get()->heightPart()*7)
                   << QPointF(View::get()->width(), View::get()->waterHeightAtStart());
        setPolygon(icePolygon);
    }

    setBrush(QBrush(QColor(235,235,235)));
    setZValue(1);

    View::get()->addToScene(this);
}

void Ice::startMelting() {
    View::get()->setIceMeltedTrue();

    _meltConnection = connect(_meltTimer, SIGNAL(timeout()),
                              this, SLOT(melt()));
    _meltTimer->customStart(_iceTimerInterval);

    View::get()->water()->toMoveUp(); // move water up
}

void Ice::melt() {
    if (View::get()->water()->rectTop() - 1 <= View::get()->heightPart() * 9) {
        // water hides ice
        _meltTimer->stop();
        QObject::disconnect(_meltConnection);
        View::get()->deleteIce();
        return;
    } else {
        QPolygonF icePolygon = polygon();
        icePolygon.translate(0, _iceStep);
        setPolygon(icePolygon.subtracted(QPolygonF(View::get()->water()->rect())));
    }
}
