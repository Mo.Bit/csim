#include "MyQPushButton.h"


MyQPushButton::MyQPushButton(const QIcon& icon, const QString& text, QWidget* parent):
    QPushButton(icon, text, parent)
{
    setFocusPolicy(Qt::NoFocus);
    setCursor(Qt::PointingHandCursor);
}
