#pragma once

#include <QPushButton>

class MyQPushButton : public QPushButton {
public:
    MyQPushButton(const QIcon& icon, const QString& text, QWidget* parent);
};
