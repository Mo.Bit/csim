#pragma once

#include <QtGlobal>

/**
 * @brief The Dim2Vector class for a 2 dimensional vector
 */
class Dim2Vector {
public:
    Dim2Vector() = default;

    /**
     * @brief Dim2Vector makes a vector from two points
     * @param x1 coordinate of first point
     * @param y1 coordinate of first point
     * @param x2 coordinate of second point
     * @param y2 coordinate of second point
     */
    Dim2Vector(const qreal& x1, const qreal& y1, const qreal& x2, const qreal& y2);

    virtual ~Dim2Vector() = default;

    /**
     * @brief length calculates the vector length
     * @return vector length
     */
    virtual qreal length() const;

    qreal x() const {return _x;}
    qreal y() const {return _y;}

    void invertX();
    void invertY();

    void setX(const qreal& x);
    void setY(const qreal& y);

protected:
    qreal _x, _y;

    /**
     * @brief makeUnit makes the vector an unit vector
     */
    void makeUnit();
};
