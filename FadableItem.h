#pragma once

#include <QAbstractGraphicsShapeItem>
#include <QTimer>
#include <QObject>
#include <QBrush>
#include <QColor>
#include <QMetaObject>

class FadableItem : public QObject {
    Q_OBJECT
public:
    FadableItem(QAbstractGraphicsShapeItem* const item, QObject* const parent);

    virtual ~FadableItem();

    virtual void fadeOut(const bool& deleteItem);

protected slots:
    virtual void fadeIn();

private slots:
    virtual void fadeOut();

private:
    QAbstractGraphicsShapeItem* _item = nullptr;

    bool _fadingIn = false;
    bool _fadingOut = false;
    bool _deleteItemAfterFadeOut = false;

    QTimer* _fadeTimer = nullptr;
    QMetaObject::Connection _fadeConnection;
};

