#include "Water.h"

#include "View.h"
#include "House.h"
#include "Tree.h"

Water::Water(QObject* const parent):
    QObject(parent),
    _moveUpTimer(new MyTimer(this))
{
    setRect(View::get()->widthPart()*5, View::get()->waterHeightAtStart(),
            View::get()->widthPart()*5, View::get()->height() - View::get()->waterHeightAtStart());
    setPen(QPen(Qt::blue));
    setBrush(QBrush(Qt::blue));
    setZValue(2);
    View::get()->addToScene(this);
}

void Water::toMoveUp() {
    _moveUpConnection = connect(_moveUpTimer, SIGNAL(timeout()),
                                       this, SLOT(moveUp()));
    _moveUpTimer->customStart(1000);
}

void Water::moveUp() {
    QRectF r = rect();
    if (r.top() - 1 > View::get()->heightPart() * 9) {
        r.setTop(r.top() - 1);
        setRect(r);
    } else {
        // reached ground height
        QObject::disconnect(_moveUpConnection);
        delete _moveUpTimer;
        _moveUpTimer = nullptr;

        View::get()->createSecondHouses();

        Tree::fell(Tree::treesLimitToHouses());
        View::get()->setNewHousesBuiltToTrue();
        if (Tree::noPlace()) {
            View::get()->addTreeButton_setDisabled(true);
        }
    }
}
