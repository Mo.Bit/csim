#pragma once

#include "MyQChart.h"

class HistoryChart : public MyQChart {
public:
    HistoryChart(QGraphicsItem* const parent, QtCharts::QSplineSeries* const series1, QtCharts::QSplineSeries* const series2,
                 const int& biggestY1Value, const int& biggestY2Value);

    void resizeX(const int& lastX);

    void setBiggerY1Range(const int& biggestY1Value);
    void setBiggerY2Range(const int& biggestY2Value);
};
