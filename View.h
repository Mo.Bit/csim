#pragma once

#include "RightSideChart.h"
#include "Factory.h"
#include "Ice.h"
#include "Water.h"
#include "MyTimer.h"
#include "MyQPushButton.h"

#include <list>

#include <QWidget>
#include <QGraphicsView>
#include <QApplication>
#include <QSize>
#include <QGraphicsScene>
#include <QScreen>
#include <QGraphicsSimpleTextItem>
#include <QGraphicsRectItem>
#include <QGraphicsPolygonItem>
#include <QGraphicsEllipseItem>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QPolygonF>
#include <QPointF>
#include <QObject>
#include <QShortcut>
#include <QKeySequence>
#include <QPushButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QLabel>
#include <QFrame>
#include <QtCharts/QChart>
#include <QListWidget>
#include <QIcon>
#include <QMessageBox>
#include <QTimer>
#include <QGraphicsSimpleTextItem>

class View : public QGraphicsView {
    Q_OBJECT
public:
    View(QWidget* const rightSideWidget, QWidget* const parent);

    ~View() override;

    static View* get() {return View::_instance;}

    QWidget* rightSideWidget() const {return _rightSideWidget;}
    QGridLayout* rightSideGridLayout() const {return _rightSideGridLayout;}

    void addToScene(QGraphicsItem* const item)  {_scene->addItem(item);}
    void removeFromScene(QGraphicsItem* const item) {_scene->removeItem(item);}

    qreal widthPart() const {return _widthPart;}
    qreal heightPart() const {return _heightPart;}
    qreal waterHeightAtStart() const {return _waterHeightAtStart;}

    RightSideChart* chart() const {return _rightSideChart;}
    Factory* factory() const {return _factory;}
    Water* water() const {return _water;}

    Ice* ice() const {return _ice;}
    bool iceMelted() const {return _iceMelted;}
    void setIceMeltedTrue() {_iceMelted = true;}
    void deleteIce();

    bool newHousesBuilt() const {return _newHousesBuilt;}
    void setNewHousesBuiltToTrue();

    /**
     * @brief addToLog adds the given text to the log
     * @param txt text to add to log
     */
    void addToLog(const QString &txt);

    // to access setDisabled()
    void addCButton_setDisabled(const bool &b);
    void addTreeButton_setDisabled(const bool &b);
    void createOrUpgradeFactoryButton_setDisabled(const bool &b);
    void destroyOrDowngradeFactoryButton_setDisabled(const bool &b);

    void createSecondHouses();

    void setFirstHousesTimers(MyTimer * const timer);

private slots:
    void resumeInit();

    void exitOrRestartFun();

    void pauseOrResumeFun();

    void speedUpFun();

private:
    static View* _instance;

    QTimer* _initTimer = nullptr;

    QWidget* _rightSideWidget = nullptr;
    QGridLayout* _rightSideGridLayout = nullptr;

    QGraphicsScene* _scene = nullptr;

    qreal _widthPart, _heightPart, _waterHeightAtStart;

    MyTimer* _createSunLightTimer = nullptr;

    MyQPushButton* _addCButton = nullptr;
    MyQPushButton* _addTreeButton = nullptr;
    MyQPushButton* _createOrUpgradeFactoryButton = nullptr;
    MyQPushButton* _destroyOrDowngradeFactoryButton = nullptr;
    MyQPushButton* _pauseOrResumeButton = nullptr;
    MyQPushButton* _speedUpButton = nullptr;
    QListWidget* _log = nullptr;

    RightSideChart* _rightSideChart = nullptr;
    Factory* _factory = nullptr;
    Ice* _ice = nullptr;
    Water* _water = nullptr;

    std::array<MyTimer*, 2> _firstHousesTimers = {nullptr, nullptr};

    bool _iceMelted = false;
    bool _newHousesBuilt = false;

    void create_Buttons_Log_Layout();
    void createSun();
    void createGround();
    void createFirstHouses();
    void createParticlesLegend();

    void resizeEvent(QResizeEvent* event) override;
};
