#pragma once

#include "Ice.h"
#include "MyTimer.h"

#include <QGraphicsRectItem>
#include <QObject>
#include <QMetaObject>
#include <QPen>
#include <QBrush>

class Water : public QObject, public QGraphicsRectItem {
    Q_OBJECT
public:
    Water(QObject* const parent);

    void toMoveUp();

    qreal rectTop() const {return rect().top();}

private slots:
    void moveUp();

private:
    MyTimer* _moveUpTimer = nullptr;
    QMetaObject::Connection _moveUpConnection;
};
