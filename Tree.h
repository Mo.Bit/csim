#pragma once

#include "MyTimer.h"
#include "FadableItem.h"

#include <vector>

#include <QGraphicsRectItem>
#include <QBrush>
#include <QPointF>
#include <QColor>
#include <QRandomGenerator>
#include <QGraphicsEllipseItem>

class Tree : public FadableItem, public QGraphicsRectItem {
    Q_OBJECT
public:
    static void setupAndAdd();

    unsigned int ownIndex() const {return _ownIndex;}

    static Tree* treePointerOnIndex(const int &index) {return Tree::_treesPointers[index];}

    static void addTree();

    static void fell(const unsigned int& limit);

    static bool noPlace();

    static unsigned int treesLimitToFactory() {return Tree::_treesLimitToFactory;}
    static unsigned int treesLimitToHouses() {return Tree::_treesLimitToHouses;}

    void cArrieved();

private slots:
    void takeC();

    void toDie();

    void die();

private:
    class TreeEllipse : public FadableItem, public QGraphicsEllipseItem {
    public:
        TreeEllipse(const qreal& x, const qreal& y, const qreal& w, const qreal& h, QObject* const parent);
    };

    unsigned int _ownIndex;

    static qreal _rectWidth;
    static unsigned int _numTrees;
    static const unsigned int _maxNumOfTrees;
    static const unsigned int _treesLimitToFactory;
    static const unsigned int _treesLimitToHouses;
    static std::vector<bool> _trees;
    static std::vector<Tree*> _treesPointers;
    static const int _takeCTimerInterval;

    unsigned int _cTaken = 0;
    TreeEllipse* _circle = nullptr;

    MyTimer* _treeTimer = nullptr;
    QMetaObject::Connection _treeConnection;

    bool _felled = false;

    Tree(const unsigned int &index);

    void moveVertically(const qreal &factor);
};
